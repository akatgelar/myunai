import React from "react";
import { Root } from "native-base";
import { StackNavigator, DrawerNavigator } from "react-navigation";


import SplashPage from "./screens/splash-screen";
import Login from "./screens/login/";
import Home from "./screens/home/";
import GantiPassword from "./screens/ganti-password";
import Jadwal from "./screens/jadwal/";
import BeritaInformasi from "./screens/berita-informasi/";
import NilaiSemester from "./screens/nilai-semester/";
import Transkrip from "./screens/transkrip/";
import Keuangan from "./screens/keuangan/";
import Absensi from "./screens/absensi/";


const Drawer = DrawerNavigator(
  {
    SplashPage: { screen: SplashPage },
    Login: { screen: Login },
    Home: { screen: Home },
    GantiPassword: { screen: GantiPassword },
    Jadwal: { screen: Jadwal },
    BeritaInformasi: { screen: BeritaInformasi },
    NilaiSemester: { screen: NilaiSemester },
    Transkrip: { screen: Transkrip },
    Keuangan: { screen: Keuangan },
    Absensi: { screen: Absensi },
  },
  {
    initialRouteName: "SplashPage",
    contentOptions: {
      activeTintColor: "#e91e63"
    }
  }
);

const AppNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;
