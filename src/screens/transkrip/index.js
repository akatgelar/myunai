import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Card, CardItem, Item, Accordion, Toast} from "native-base";
// const dataArray = [
//   { title: ["Semester 1", "IP 3.6", "SKS 20"], content: [["Sistem Enterprise", "A"], ["Bahasa Indonesia", "A"], ["Etika Profesi", "A"], ["Basis Data", "A"]] },
//   { title: ["Semester 2", "IP 3.6", "SKS 20"], content: [["Sistem Enterprise", "A"], ["Bahasa Indonesia", "A"], ["Etika Profesi", "A"], ["Basis Data", "A"]] },
//   { title: ["Semester 3", "IP 3.6", "SKS 20"], content: [["Sistem Enterprise", "A"], ["Bahasa Indonesia", "A"], ["Etika Profesi", "A"], ["Basis Data", "A"]] },
//   { title: ["Semester 4", "IP 3.6", "SKS 20"], content: [["Sistem Enterprise", "A"], ["Bahasa Indonesia", "A"], ["Etika Profesi", "A"], ["Basis Data", "A"]] },
// ];

class Transkrip extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      transkrip: [],
      detail: [],
      total: [],
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // jadwal
    const URL2 = "http://10.0.2.2/myunai/transkrip.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({transkrip: json.data});
          this.setState({detail: json.data.detail});
          this.setState({total: json.data.total});
          // console.log(this.state.transkrip);
          // console.log(this.state.detail);
          // console.log(this.state.total);
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon type="FontAwesome" name="list-alt" style={{color: "white", marginRight: 10}}/>
            <Title>Transkrip</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>
          <Card>
            <CardItem header bordered>
              <Left>
                <Text style={{fontSize: 20, fontWeight: "bold"}}>{this.state.total[0]}</Text>
              </Left>
              <Item>
                <Text style={{fontSize: 20, fontWeight: "bold"}}>Total {this.state.total[1]}</Text>
              </Item>
            </CardItem>
          </Card>

          <Card>
          <Accordion dataArray={this.state.detail} renderHeader={(title) =>
            <Card transparent>
              <CardItem style={{ backgroundColor: "white", borderWidth:0}}>
                <Left>
                  <Text>{title.title[0]}</Text>
                  <Text>{title.title[1]}</Text>
                  <Text>{title.title[2]}</Text>
                </Left>
                <Right>
                  <Icon type="MaterialIcons" name="keyboard-arrow-right" color="black"></Icon>
                </Right>
              </CardItem>
            </Card>}
            renderContent={(content) => {
              let cards = content.content.map((item, index) => {
                return <Card transparent key={index} style={{marginTop:-7}}>
                  <CardItem style={{ backgroundColor: "#EDEDF2" }}>
                    <Left><Text>{item[0]}</Text></Left>
                    <Right><Text style={{fontWeight:"bold"}}>{item[1]}</Text></Right>
                  </CardItem>
                </Card>
              });
              return cards;
            }} />
          </Card>

        </Content>
      </Container>

    );
  }
}

export default Transkrip;
