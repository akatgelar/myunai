import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Card, Toast} from "native-base";

class BeritaInformasi extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      berita: [],
      informasi: [],
      semua: [],
      data: [],
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // berita
    const URL1 = "http://10.0.2.2/myunai/berita.php";
    fetch(URL1)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({data: json.data});
          // console.log(this.state.data);
          // console.log(this.state.informasi);
          this.loopJadwal();
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  loopJadwal() {

    var count = this.state.data.length;
    // console.log(this.state.data);

    var ber = [];
    for (let i = 0; i < count; i++){
      ber.push(
        <Card style={{padding:10}} key = {i}>
          <Text style={{marginLeft:10, marginTop: 5, fontWeight: "bold"}}>{this.state.data[i].berita}</Text>
          <Text style={{marginLeft:10, marginTop: 5, fontWeight: "bold"}}>{this.state.data[i].pengumuman}</Text>
        </Card>
      );
    }

    this.setState({semua: ber});
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon type="FontAwesome" name="newspaper-o" style={{color: "white", marginRight: 10}}/>
            <Title>Berita Informasi</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>
          {this.state.semua}
        </Content>
      </Container>

    );
  }
}

export default BeritaInformasi;
