import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Card, CardItem, List, ListItem, Thumbnail, Toast } from "native-base";
import { Grid, Col } from "react-native-easy-grid";

class Jadwal extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      jadwal: [],
      jadwal_semua: [],
      hari: "senin",
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // jadwal
    const URL2 = "http://10.0.2.2/myunai/jadwal.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({jadwal: json.data});
          // console.log(this.state.jadwal);
          this.loopJadwal();
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  loopJadwal() {

    // console.log(this.state.jadwal);

    // get hari
    var d = new Date();
    var weekday = new Array(8);
    weekday[1] = "senin";
    weekday[2] = "selasa";
    weekday[3] = "rabu";
    weekday[4] = "kamis";
    weekday[5] = "jumat";
    weekday[6] = "sabtu";
    weekday[7] = "minggu";
    // this.state.hari = weekday[d.getDay()];

    var _cards = [];
    for (let j = 1; j < 6; j++){

      var _listitem = [];
      var count = this.state.jadwal[weekday[j]].length;
      // console.log(count);
      // console.log(this.state.jadwal[weekday[j]]);
      for (let i = 0; i < count; i++){
        _listitem.push(
          <ListItem key={Math.random()}>
            <Grid>
              <Col size={3}>
                <Text note style={{fontSize: 10}}>{this.state.jadwal[weekday[j]][i].jadwal_jam_mulai} - {this.state.jadwal[weekday[j]][i].jadwal_jam_selesai}</Text>
              </Col>
              <Col size={8}>
                <Text style={{fontSize: 12}}>{this.state.jadwal[weekday[j]][i].matkul_nama} ({this.state.jadwal[weekday[j]][i].matkul_kode})</Text>
                <Text note style={{fontSize: 10}}>{this.state.jadwal[weekday[j]][i].dosen_nama}</Text>
              </Col>
              <Col size={2}>
                <Text note style={{fontSize: 10}}>{this.state.jadwal[weekday[j]][i].ruangan_kode}</Text>
              </Col>
            </Grid>
          </ListItem>
        );
      }
      
      _cards.push(
        <Card  key={Math.random()}>
          <Text style={{marginLeft:10, marginTop: 5, marginBottom: 5}}>{weekday[j]}</Text>
          <List>
            {_listitem}
          </List>
        </Card>
      );
    }

    this.setState({jadwal_semua: _cards});
  }


  
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon type="FontAwesome" name="calendar" style={{color: "white", marginRight: 10}}/>
            <Title>Jadwal</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>
          {this.state.jadwal_semua}
        </Content>
      </Container>

    );
  }
}

export default Jadwal;
