export default {
spinnerTextStyle: {
    color: "#FFF"
    },
    container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
    },
};
