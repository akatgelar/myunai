import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Card, CardItem, Body, Text, Header, Button, Icon, Footer, FooterTab, ActionSheet, Toast } from "native-base";
import { Grid, Col } from "react-native-easy-grid";

const img_user = require("../../../assets/img_user.png");
var BUTTONS = ["Ganti Password", "Logout", "Cancel"];
var CANCEL_INDEX = 2;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      berita: [],
      informasi: [],
      jadwal: [],
      jadwal_sekarang: [],
      hari: "senin",
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // get hari
    var d = new Date();
    var weekday = new Array(8);
    weekday[1] = "senin";
    weekday[2] = "selasa";
    weekday[3] = "rabu";
    weekday[4] = "kamis";
    weekday[5] = "jumat";
    weekday[6] = "sabtu";
    weekday[7] = "minggu";
    this.setState({hari:"rabu"});
    // this.setState({hari:weekday[d.getDay()]});

    // berita
    const URL1 = "http://10.0.2.2/myunai/berita.php";
    fetch(URL1)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({berita: json.data[0].berita});
          this.setState({informasi: json.data[0].informasi});
          // console.log(this.state.berita);
          // console.log(this.state.informasi);
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});


    // jadwal
    const URL2 = "http://10.0.2.2/myunai/jadwal.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({jadwal: json.data});
          // console.log(this.state.jadwal);
          this.loopJadwal();
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  loopJadwal() {

    var count = this.state.jadwal[this.state.hari].length;
    // console.log(this.state.jadwal[this.state.hari]);

    var jadwals = [];
    for (let i = 0; i < count; i++){
      jadwals.push(
        <View key = {i}>
          <View>
            <Text>{this.state.jadwal[this.state.hari][i].jadwal_hari}, {this.state.jadwal[this.state.hari][i].jadwal_jam_mulai} - {this.state.jadwal[this.state.hari][i].jadwal_jam_selesai} ({this.state.jadwal[this.state.hari][i].ruangan_kode})</Text>
            <Text>{this.state.jadwal[this.state.hari][i].matkul_kode} - {this.state.jadwal[this.state.hari][i].matkul_nama}</Text>
            <Text>{this.state.jadwal[this.state.hari][i].dosen_kode} - {this.state.jadwal[this.state.hari][i].dosen_nama}</Text>
          </View>
        </View>
      );
    }

    this.setState({jadwal_sekarang: jadwals});
  }

  render() {


    return (
      <Container>
        <Header style={{ height: 80 }}>
          <Body>
            <Grid>
              <Col size={3}>
                <Image
                  style={{
                    margin: 10,
                    width: 60,
                    height: 0,
                    flex: 1,
                    borderRadius: 40,
                    borderWidth: 1
                  }}
                  source={img_user}>
                </Image>
              </Col>
              <Col size={9} style={{ justifyContent: "center"}}>
                <Text style={{color: "white"}}>{this.state.nama}</Text>
                <Text style={{color: "white"}}>{this.state.nim}</Text>
              </Col>
            </Grid>
          </Body>
        </Header>

        <Content padder>

          <Card>
            <CardItem style={{justifyContent: "center"}}>
                <Button primary
                  onPress={() => this.props.navigation.navigate("NilaiSemester")}
                  style={{
                    justifyContent: "center",
                    flexDirection: "column",
                    borderRadius: 55,
                    height: 55,
                    width: 55,
                    marginRight: 5
                  }}>
                  <Icon style={{fontSize:23}} active type="FontAwesome" name="file-text-o" />
                  <Text style={{fontSize: 6, textAlign: "center", lineHeight: 7, marginTop: 3, marginLeft: -5, marginRight: -5, fontWeight: "bold"}}>{"Nilai\nSemester"}</Text>
                </Button>
                <Button success
                  onPress={() => this.props.navigation.navigate("Transkrip")}
                  style={{
                    justifyContent: "center",
                    flexDirection: "column",
                    borderRadius: 55,
                    height: 55,
                    width: 55,
                    marginRight: 5
                  }}>
                  <Icon style={{fontSize:23}} active type="FontAwesome" name="list-alt" />
                  <Text style={{fontSize: 6, textAlign: "center", lineHeight: 7, marginTop: 3, marginLeft: -5, marginRight: -5, fontWeight: "bold"}}>{"Transkrip"}</Text>
                </Button>
                <Button warning
                  onPress={() => this.props.navigation.navigate("Keuangan")}
                  style={{
                    justifyContent: "center",
                    flexDirection: "column",
                    borderRadius: 55,
                    height: 55,
                    width: 55,
                    marginRight: 5
                  }}>
                  <Icon style={{fontSize:23}} active type="FontAwesome" name="credit-card-alt" />
                  <Text style={{fontSize: 6, textAlign: "center", lineHeight: 7, marginTop: 3, marginLeft: -5, marginRight: -5, fontWeight: "bold"}}>{"Keuangan"}</Text>
                </Button>
                <Button danger
                  onPress={() => this.props.navigation.navigate("Absensi")}
                  style={{
                    justifyContent: "center",
                    flexDirection: "column",
                    borderRadius: 55,
                    height: 55,
                    width: 55,
                    marginRight: 5
                  }}>
                  <Icon style={{fontSize:23}} active type="FontAwesome" name="bookmark" />
                  <Text style={{fontSize: 6, textAlign: "center", lineHeight: 7, marginTop: 3, marginLeft: -5, marginRight: -5, fontWeight: "bold"}}>{"Absensi"}</Text>
                </Button>
                <Button dark
                  onPress={() => this.props.navigation.navigate("BeritaInformasi")}
                  style={{
                    justifyContent: "center",
                    flexDirection: "column",
                    borderRadius: 55,
                    height: 55,
                    width: 55
                  }}>
                  <Icon style={{fontSize:23}} active type="FontAwesome" name="newspaper-o" />
                  <Text style={{fontSize: 6, textAlign: "center", lineHeight: 7, marginTop: 3, marginLeft: -5, marginRight: -5, fontWeight: "bold"}}>{"Berita\nInformasi"}</Text>
                </Button>
            </CardItem>
          </Card>

          <Card>
            <CardItem header>
              <Text>Jadwal</Text>
            </CardItem>
            <CardItem style={{paddingLeft: 50}}>
              <Body>
                {this.state.jadwal_sekarang}
              </Body>
            </CardItem>
            <CardItem style={{flexDirection: "row", justifyContent: "flex-end"}}>
                <Button iconRight transparent
                  onPress={() => this.props.navigation.navigate("Jadwal")}>
                  <Text>Lihat Selengkapnya</Text>
                  <Icon type="FontAwesome" name="angle-double-right" />
                </Button>
            </CardItem>
          </Card>

          <Card>
            <CardItem header>
              <Text>Berita dan Informasi</Text>
            </CardItem>
            <CardItem style={{paddingLeft: 50}}>
              <Body>
                <Text>{this.state.berita}</Text>
                <Text>{this.state.informasi}</Text>
              </Body>
            </CardItem>
            <CardItem style={{flexDirection: "row", justifyContent: "flex-end"}}>
                <Button iconRight transparent
                  onPress={() => this.props.navigation.navigate("BeritaInformasi")}>
                  <Text>Lihat Selengkapnya</Text>
                  <Icon type="FontAwesome" name="angle-double-right" />
                </Button>
            </CardItem>
          </Card>

          <View>
            <Text>{"\n"}</Text>
          </View>


        </Content>

        <Footer>
          <FooterTab>
            <Button vertical
              onPress={() => this.props.navigation.navigate("Jadwal")}>
              <Icon type="FontAwesome" name="calendar" />
              <Text>Jadwal</Text>
            </Button>
            <Button vertical active>
              <Icon type="FontAwesome" name="home" />
              <Text>Home</Text>
            </Button>
            <Button vertical="true"
              onPress={() =>
                ActionSheet.show(
                {
                  options: BUTTONS,
                  cancelButtonIndex: CANCEL_INDEX,
                  title: "Lainnya"
                },
                buttonIndex => {
                  if (buttonIndex === 0) {
                    this.props.navigation.navigate("GantiPassword");
                  } else if (buttonIndex === 1) {
                    this.props.navigation.navigate("Login");
                  } else {
                    // console.log("do nothing");
                  }
                }
              )}>
              <Icon type="FontAwesome" active name="navicon" />
              <Text>Lainnya</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}

export default Home;
