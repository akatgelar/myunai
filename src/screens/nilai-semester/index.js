import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Card, CardItem, Grid, Col, Toast} from "native-base";

class NilaiSemester extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      transkrip: [],
      detail: [],
      total: [],
      nilai_semua: [],
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // jadwal
    const URL2 = "http://10.0.2.2/myunai/transkrip.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({transkrip: json.data});
          this.setState({detail: json.data.detail});
          this.setState({total: json.data.total});
          // console.log(this.state.transkrip);
          // console.log(this.state.total);
          console.log(this.state.detail);
          this.loopJadwal();
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  loopJadwal() {

    // console.log(this.state.detail);
    var semester = this.state.detail.length;
    var _cards = [];
    for (let j = 0; j < semester; j++){

      var _listitem = [];
      var count = this.state.detail[j].content.length;
      // console.log(count);
      // console.log(this.state.jadwal[weekday[j]]);
      for (let i = 0; i < count; i++){
        _listitem.push(
          <CardItem key={Math.random()} bordered style={{flexDirection: "column", marginTop: -20}}>
            <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>{this.state.detail[j].content[i][0]}</Text>
            <Text style={{textDecorationLine: "underline"}}>Total</Text>
            <Text style={{fontSize: 30}}>{this.state.detail[j].content[i][1]}</Text>
          </CardItem>
        );
      }
      
      _cards.push(
        <Card key={Math.random()}>
          <CardItem header bordered>
            <Text>{this.state.detail[j].title[0]}</Text>
          </CardItem>
          {_listitem}
        </Card>
      );
    }

    this.setState({nilai_semua: _cards});
  }


  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon type="FontAwesome" name="file-text-o" style={{color: "white", marginRight: 10}}/>
            <Title>Nilai Semester</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>
          {this.state.nilai_semua}
          {/* <Card>
            <CardItem header bordered>
              <Text>2018/2019 Ganjil</Text>
            </CardItem>
            <CardItem bordered style={{flexDirection: "column", marginTop: -20}}>
              <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>Sistem Informasi Enterprise</Text>
              <Grid bordered>
                <Col size={5} style={{ alignItems: "center", borderTopWidth: 1, borderRightWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>A</Text>
                </Col>
                <Col size={5} style={{ alignItems: "center", borderTopWidth: 1, borderLeftWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>B</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem bordered style={{flexDirection: "column", marginTop: -20}}>
              <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>Konsep Analisa & E-Bisnis</Text>
              <Grid bordered>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderRightWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>B</Text>
                </Col>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderLeftWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>A</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem bordered style={{flexDirection: "column", marginTop: -20}}>
              <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>Perancangan dan Manajemen Jaringan</Text>
              <Grid bordered>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderRightWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>A</Text>
                </Col>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderLeftWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>B</Text>
                </Col>
              </Grid>
            </CardItem>
          </Card>

          <Card>
            <CardItem header bordered>
              <Text>2018/2019 Genap</Text>
            </CardItem>
            <CardItem bordered style={{flexDirection: "column", marginTop: -20}}>
              <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>Sistem Informasi Enterprise</Text>
              <Grid bordered>
                <Col size={5} style={{ alignItems: "center", borderTopWidth: 1, borderRightWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>A</Text>
                </Col>
                <Col size={5} style={{ alignItems: "center", borderTopWidth: 1, borderLeftWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>B</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem bordered style={{flexDirection: "column", marginTop: -20}}>
              <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>Konsep Analisa & E-Bisnis</Text>
              <Grid bordered>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderRightWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>B</Text>
                </Col>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderLeftWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>A</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem bordered style={{flexDirection: "column", marginTop: -20}}>
              <Text style={{marginBottom:10, marginTop: 10, fontWeight: "bold"}}>Perancangan dan Manajemen Jaringan</Text>
              <Grid bordered>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderRightWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>A</Text>
                </Col>
                <Col size={5} style={{ alignItems: "center", borderTopWidth:1, borderLeftWidth:1, borderColor:"black", paddingTop: 10}}>
                  <Text style={{textDecorationLine: "underline"}}>UTS</Text>
                  <Text style={{fontSize: 30}}>B</Text>
                </Col>
              </Grid>
            </CardItem>
          </Card> */}

        </Content>
      </Container>

    );
  }
}

export default NilaiSemester;
