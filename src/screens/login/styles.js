export default {
  logoContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 0,
  },
  logo: {
    position: "absolute",
    width: 150,
    height: 160
  }
};
