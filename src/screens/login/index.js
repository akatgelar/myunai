import React, { Component } from "react";
import { ImageBackground, View, StatusBar, AsyncStorage } from "react-native";
import { Container, Content, Button, Input, Item, Form, Text, Toast, Spinner } from "native-base";

import styles from "./styles";

const logo = require("../../../assets/icon_app.png");

class Login extends Component {

  constructor(props){
    super(props);
    this.state = { 
      isLoading: false,
      username: "",
      password: ""
    };
    // this.loadData();
  }

  loadData() {

    this.state.isLoading = true;
    const URL = "http://10.0.2.2/myunai/login.php?username=" + this.state.username + "&password=" + this.state.password;
    this.getDataAPI(URL).then((res) => {
      this.state.isLoading = false;
      // console.log(res);
      if (res.code === "1") {
        AsyncStorage.multiSet([
          ["nim", res.data.nim],
          ["nama", res.data.nama]
        ]);
        this.props.navigation.navigate("Home");
      } else {
        Toast.show({
          text: res.msg,
          buttonText: "Ok"
        });
      }
    }).catch(function(error) {
      console.log("There has been a problem with your fetch operation: " + error.message);
      throw error;
    });
  }

  getDataAPI(URL) {
    return fetch(URL)
    .then((res) => res.json());
  }

  render() {
    return (
      <Container>
        <StatusBar barStyle="dark-content" />
          <View style={styles.logoContainer}>
            <ImageBackground source={logo} style={styles.logo} />
          </View>
          <Content padder>
            <Form>
              <Item regular
                style={{
                  borderStyle: "solid",
                  borderColor: "black"
                }}>
                <Input placeholder="Username" name="username" onChangeText={(text) => this.setState({username: text})} />
              </Item>
              <Item regular
                style={{
                  marginTop: 5,
                  borderStyle: "solid",
                  borderColor: "black"
                }}>
                <Input placeholder="Password" name="password" onChangeText={(text) => this.setState({password: text})} secureTextEntry/>
              </Item>
            </Form>
            <Button block success
              style={{
                marginTop: 20
              }}
              onPress={this.loadData.bind(this)}
              >
              <Text>Login</Text>
            </Button>
          </Content>
      </Container>
    );
  }
}

export default Login;
