import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Item, Label, Form, Input, Toast } from "native-base";

class GantiPassword extends Component {
  
  constructor(props){
    super(props);
    this.state = { 
      isLoading: false,
      password1: "",
      password2: "",
      nim: "",
      nama: ""
    };
    // this.loadData();

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

    });
  }

  loadData() {

    if (this.state.password1 !== this.state.password2) {

      Toast.show({ text: "Password Tidak Sama!", buttonText: "Ok" });

    } else {

      this.state.isLoading = true;
      const URL = "http://10.0.2.2/myunai/password.php?nim=" + this.state.nim + "&password=" + this.state.password1;
      this.getDataAPI(URL).then((res) => {
        this.state.isLoading = false;
        // console.log(res);
        if (res.code === "1") {
          
          Toast.show({ text: "Password Sukses Diganti!", buttonText: "Ok" });
          this.props.navigation.navigate("Login");
        } else {
          Toast.show({
            text: res.msg,
            buttonText: "Ok"
          });
        }
      }).catch(function(error) {
        console.log("There has been a problem with your fetch operation: " + error.message);
        throw error;
      });

    }
  }

  getDataAPI(URL) {
    return fetch(URL)
    .then((res) => res.json());
  }


  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon name="key" style={{color: "white", marginRight: 10}}/>
            <Title>Ganti Password</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>
          <Form style={{paddingTop: 80}}>
            <Label style={{fontWeight: "bold", marginTop: 10}}>Password Lama</Label>
            <Item regular
              style={{
                borderStyle: "solid",
                borderColor: "black"
              }}>
              <Input placeholder="Password Lama" secureTextEntry/>
            </Item>

            <Label style={{fontWeight: "bold", marginTop: 10}}>Password Baru</Label>
            <Item regular
              style={{
                borderStyle: "solid",
                borderColor: "black"
              }}>
              <Input placeholder="Password Baru" name="password1" onChangeText={(text) => this.setState({password1: text})} secureTextEntry/>
            </Item>

            <Label style={{fontWeight: "bold", marginTop: 10}}>Konfirmasi Password Baru</Label>
            <Item regular
              style={{
                marginTop: 5,
                borderStyle: "solid",
                borderColor: "black"
              }}>
              <Input placeholder="Konfirmasi Password Baru" name="password2" onChangeText={(text) => this.setState({password2: text})} secureTextEntry/>
            </Item>
          </Form>
          <Button block primary
            style={{
              marginTop: 20
            }}
            onPress={this.loadData.bind(this)}>
            <Text>Ganti Password</Text>
          </Button>

        </Content>
      </Container>

    );
  }
}

export default GantiPassword;
