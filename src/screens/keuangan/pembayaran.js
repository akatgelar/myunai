import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Grid, Col, Card, CardItem, Item, Accordion, Tab, Tabs, TabHeading, Toast} from "native-base";


class Pembayaran extends Component {

  
  constructor(props) {
    super(props);
    this.state = { 
      keuangan: [],
      pembayaran: [],
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // jadwal
    const URL2 = "http://10.0.2.2/myunai/keuangan.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({keuangan: json.data});
          console.log(this.state.keuangan);
          this.loopJadwal();
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  
  loopJadwal() {

    // console.log(this.state.keuangan);
    var count = this.state.keuangan.length;
    var bayar = [];
    for (let j = 0; j < count; j++){
      bayar.push(
        <Card key={j}>
          <CardItem bordered>
            <Grid>
              <Col size={4}>
                <Text note style={{fontSize: 12}}>{this.state.keuangan[j].id_keuangan}</Text>
              </Col>
              <Col size={8}>
                <Text style={{fontSize: 12}}>Keterangan {this.state.keuangan[j].id_keuangan}</Text>
              </Col>
              <Col size={5}>
                <Text note style={{fontSize: 12}}>Rp. {this.numberWithCommas(this.state.keuangan[j].debit)}</Text>
              </Col>
            </Grid>
          </CardItem>
        </Card>
      );
    }

    this.setState({pembayaran: bayar});
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }


  render() {
    return (
      <Container>

        <Content>
          {this.state.pembayaran}

        </Content>
      </Container>

    );
  }
}

export default Pembayaran;
