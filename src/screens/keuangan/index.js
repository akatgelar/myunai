import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Card, CardItem, Item, Accordion, Toast, Tab, Tabs, TabHeading} from "native-base";
import Tagihan from "./tagihan";
import Pembayaran from "./pembayaran";

class Keuangan extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      keuangan: [],
      debit: "",
      kredit: "",
      total: "",
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // jadwal
    const URL2 = "http://10.0.2.2/myunai/keuangan.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({keuangan: json.data});
          // console.log(this.state.keuangan);
          this.loopJadwal();
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  
  loopJadwal() {

    // console.log(this.state.keuangan);
    var count = this.state.keuangan.length;
    var debit_total = 0;
    var kredit_total = 0;
    var total_total = 0;
    for (let j = 0; j < count; j++){
      debit_total = debit_total + Number(this.state.keuangan[j].debit);
      kredit_total = kredit_total + Number(this.state.keuangan[j].kredit);
      total_total = total_total + (debit_total - kredit_total);
    }

    this.setState({debit: this.numberWithCommas(debit_total)});
    this.setState({kredit: this.numberWithCommas(kredit_total)});
    this.setState({total: this.numberWithCommas(total_total)});
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon type="FontAwesome" name="credit-card-alt" style={{color: "white", marginRight: 10}}/>
            <Title>Keuangan</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>
          <Card>
            <CardItem header bordered>
              <Body style={{alignItems:"center"}}>
                <Text style={{fontSize: 20}}>Saldo Akhir</Text>
                <Text style={{fontSize: 20, fontWeight: "bold"}}>Rp. {this.state.total}</Text>
              </Body>
            </CardItem>
          </Card>

          <Card>
            <Tabs style={{backgroundColor:"white"}}>
              <Tab heading={ <TabHeading><Text>Tagihan</Text></TabHeading>}>
                <Tagihan />
              </Tab>
              <Tab heading={ <TabHeading><Text>Pembayaran</Text></TabHeading>}>
                <Pembayaran />
              </Tab>
            </Tabs>
          </Card>

        </Content>
      </Container>

    );
  }
}

export default Keuangan;
