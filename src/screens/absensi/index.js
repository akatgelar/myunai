import React, { Component } from "react";
import { Image, View, AsyncStorage } from "react-native";
import { Container, Content, Body, Text, Header, Button, Icon, Left, Right, Title, Card, CardItem, Item, Accordion, Badge, Toast} from "native-base";
// const dataArray = [
//   { title: ["Konsep Analisa & Bisnis", "20", "1", "2", "3"], content: [["20 Agustus 2018", "Hadir"], ["21 Agustus 2018", "Alfa"], ["22 Agustus 2018", "Izin"], ["23 Agustus 2018", "Sakit"], ["24 Agustus 2018", "Hadir"], ["25 Agustus 2018", "Hadir"], ["26 Agustus 2018", "Hadir"], ["27 Agustus 2018", "Hadir"], ["28 Agustus 2018", "Hadir"]] },
//   { title: ["Manajemen Data & Informasi", "20", "1", "2", "3"], content: [["20 Agustus 2018", "Hadir"], ["21 Agustus 2018", "Alfa"], ["22 Agustus 2018", "Izin"], ["23 Agustus 2018", "Sakit"], ["24 Agustus 2018", "Hadir"], ["25 Agustus 2018", "Hadir"], ["26 Agustus 2018", "Hadir"], ["27 Agustus 2018", "Hadir"], ["28 Agustus 2018", "Hadir"]] },
//   { title: ["Multimedia", "20", "1", "2", "3"], content: [["20 Agustus 2018", "Hadir"], ["21 Agustus 2018", "Alfa"], ["22 Agustus 2018", "Izin"], ["23 Agustus 2018", "Sakit"], ["24 Agustus 2018", "Hadir"], ["25 Agustus 2018", "Hadir"], ["26 Agustus 2018", "Hadir"], ["27 Agustus 2018", "Hadir"], ["28 Agustus 2018", "Hadir"]] },
// ];

class Absensi extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      absensi: [],
      detail: [],
      total: [],
      nim: "",
      nama: ""
    };

    AsyncStorage.multiGet(["nim", "nama"]).then((data) => {
      let nim = data[0][1];
      let nama = data[1][1];
      this.state.nim = nim;
      this.state.nama = nama;

      this.loadData(nim, nama);
    });

  }

  loadData(nim, nama) {

    // jadwal
    const URL2 = "http://10.0.2.2/myunai/absensi.php?nim=" + nim;
    fetch(URL2)
      .then(res => res.json())
      .then(json => {
        if (json.code === "1") {
          this.setState({absensi: json.data});
          // console.log(this.state.absensi);
        } else {
          Toast.show({ text: json.msg, buttonText: "Ok" });
        }
      })
      .catch(error => { console.log("There has been a problem with your fetch operation: " + error.message); throw error;});

  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
            <Icon type="FontAwesome" name="bookmark" style={{color: "white", marginRight: 10}}/>
            <Title>Absensi</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content padder>

          <Card>
          <Accordion dataArray={this.state.absensi} renderHeader={(title) =>
            <Card transparent>
              <CardItem style={{ backgroundColor: "white", borderWidth:0}}>
                <Body>
                  <Text style={{fontSize: 17, fontWeight: "bold"}}>{title.title[0]}</Text>
                  <View style={{ flexDirection: "row"}}>
                    <Badge success><Text>H</Text></Badge>
                    <Text style={{marginRight: 10}}> {title.title[1]}</Text>
                    <Badge danger><Text>A</Text></Badge>
                    <Text style={{marginRight: 10}}> {title.title[2]}</Text>
                    <Badge info><Text>I</Text></Badge>
                    <Text style={{marginRight: 10}}> {title.title[3]}</Text>
                    <Badge warning><Text>S</Text></Badge>
                    <Text style={{marginRight: 10}}> {title.title[4]}</Text>
                  </View>
                  
                </Body>
              </CardItem>
            </Card>}
            renderContent={(content) => {
              let cards = content.content.map((item, index) => {
                return <Card transparent key={index} style={{marginTop:-10}}>
                  <CardItem style={{ backgroundColor: "#EDEDF2" }}>
                    <Left><Text>{item[0]}</Text></Left>
                    <Right><Text style={{fontWeight:"bold"}}>{item[1]}</Text></Right>
                  </CardItem>
                </Card>
              });
              return cards;
            }} />
          </Card>

        </Content>
      </Container>

    );
  }
}

export default Absensi;
