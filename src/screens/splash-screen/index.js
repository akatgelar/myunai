import React, { Component } from "react";
import { View, Image, Text } from "react-native";

const splashscreen = require("../../../assets/icon.png");

export default class SplashPage extends Component {

  constructor(props)
  {
      super(props);

  }

  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => {
          resolve("result");
        },2000
      )
    );
  }

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();
    // console.log("after splash");
    // console.log(data);

    if (data !== null) { 
      this.props.navigation.navigate("Login");
    }
  }

  render() {
    // eslint-disable-line class-methods-use-this
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          height: "100%"
        }}>
        <Image
          source={splashscreen}
          style={{
            justifyContent: "center",
            alignItems: "center",
            width: 150,
            height: 150
          }}>
        </Image>
        <Text
          style={{
            backgroundColor: "transparent",
            color: "black",
            fontWeight: "bold",
            marginTop: 20
          }}>
          My UNAI App
        </Text>
      </View>
    );
  }
}
